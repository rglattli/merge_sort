import time

class Node:
    def __init__(self, data, link=None):
            self.data = data
            self.link = link

    def insert_node(self, previous_node):
            self.link = previous_node.link
            previous_node.link = self

def runtime(n):
    #quickly create nodes
    dct = {}
    for i in range(1,n):
        dct['e%s' % i] = i

        nodename = list(dct.keys())
        e0 = Node(0)
    #create long linked list using previously created nodes
    for i in nodename:
        i = Node(0)
        Node.insert_node(i,e0)

    #calculating the runtime of inserting a node in this list
    start = time.time()
    e90 = Node(0)
    Node.insert_node(e90,e0)
    end = time.time()
    print(end - start)

listsize = [50,500,1000]

for n in listsize:
    runtime(n)







