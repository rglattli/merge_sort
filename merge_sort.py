import time
import random

def merge_sort(list_):
    if len(list_) <= 1:
        return list_

    left = merge_sort(list_[:len(list_)//2])
    right = merge_sort(list_[len(list_)//2:])

    return reduce(left,right)


def reduce(left,right):
    result = []
    left_index = 0
    right_index = 0
    while True:
        if left[left_index] < right[right_index]:
            result.append(left[left_index])
            left_index = left_index + 1

        else:
            result.append(right[right_index])
            right_index = right_index + 1

        if right_index == len(right):
            result.extend(left[left_index:])
            break

        if left_index == len(left):
            result.extend(right[right_index:])
            break

    return result