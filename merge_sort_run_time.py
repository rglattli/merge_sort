import time
import random
import merge_sort

list_size = [1001,2001,4001,8001,160001,320001]

#Best Case Scenario
print("""

Best Cases Scenario""")

print("sorted list")
for e in list_size:
    x = list(range(1,e))
    time1 = time.time()
    merge_sort.merge_sort(x)
    time2 = time.time()
    print(time2 - time1)

print("reverse sorted list")
for e in list_size:
    x = list(range(1,e))
    x.reverse()
    time1 = time.time()
    merge.sort.merge_sort(x)
    time2 = time.time()
    print(time2 - time1)

print("""
Worst Case Scenario""")
for e in list_size:
    x = list(range(1,e))
    random.shuffle(x)
    time1 = time.time()
    merge_sort.merge_sort(x)
    time2 = time.time()
    print(time2 - time1)